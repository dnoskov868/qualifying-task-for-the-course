window.addEventListener('load', function() {

    const word = document.getElementById('word'),
          excel = document.getElementById('excel'),
          powerpoint = document.getElementById('powerpoint'),
          onedrive = document.getElementById('onedrive'),
          outlook = document.getElementById('outlook'),
          mTeams = document.getElementById('microsoft-teams');

    const arrFirstSatellite = [];
    const arrSecondSatellite = [];

    arrFirstSatellite.push(word, excel, powerpoint);
    arrSecondSatellite.push(onedrive, outlook, mTeams)

    const r1 = 150, r2 = 250;

    arrFirstSatellite.forEach((item, i) => {
        animationSatellite(item, r1, i*2)
    })

    arrSecondSatellite.forEach((item, i) => {
        animationSatellite(item, r2, i*2)
    })

    function animationSatellite(satellite, r, position) {
        let pos = position;
        setInterval(() => {
            pos += 0.001;
            satellite.style.left = `${r * Math.cos(pos) + r-30}px`;
            satellite.style.top =`${r * Math.sin(pos) + r-30}px`;
        }) 
        
    }

});